import math
import os
import random
import time

from art import text2art
from ai import ai


def input_safe(func):
    """
    This is a decorator to ensure that the input is valid and won't just crash the game
    because that is extremely annoying
    :param func:
    :return: Wrapper
    """
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            print("Invalid Input, Inputs should always be numbers prompted by the game")
            time.sleep(2)

    return wrapper


class PlayingCard:

    def __init__(self, amount_of_decks: int) -> None:
        """
        This class serves as the deck of cards for the game, it includes the functions to draw
        cards as well as discard cards.
        Cards are defined here as a number between 101 and 413, the 100's place represents the suit, with the
        order being 1: spade, 2: heart, 3: club, 4: diamond. The 10's place represents the card value itself
        1: ace, 2-10: representative numbers, 11: jack, 12: queen, 13: king
        :param amount_of_decks: the number of 52 card decks to be shuffled in
        """
        self.cards = []
        for _ in range(amount_of_decks):
            for suit in range(1, 5):
                for number in range(1, 14):
                    self.cards.append((suit * 100) + number)
        random.shuffle(self.cards)
        self.card_on_top = self.cards[0]
        self.cards.pop(0)
        self.discard_pile = []

    def draw_unknown_card(self) -> int:
        card_to_draw = self.cards[0]
        self.cards.pop(0)
        return card_to_draw

    def take_the_known_card(self) -> int:
        card_to_draw = self.card_on_top
        self.card_on_top = self.cards[0]
        self.cards.pop(0)
        return card_to_draw

    def discard(self, card_to_discard):
        self.discard_pile.append(card_to_discard)

    def check_empty_deck(self):
        if len(self.cards) < 3:
            self.cards = self.cards + self.discard_pile
            random.shuffle(self.cards)
            print("The deck is empty, the discard pile is reshuffled")

    def deal_deck(self) -> list[int]:
        r_cards = self.cards[:10]
        self.cards = self.cards[10:]
        return r_cards

    def __repr__(self) -> str:
        suit = self.card_on_top
        suit = suit // 100
        card = self.card_on_top
        card = card - suit * 100
        card_suits = ["♠", "♡", "♣", "♢"]
        return f"The card on the table is the {card} of {card_suits[suit - 1]} "


class playerDeck:
    def __init__(self, named_player, deck, human_player=True):
        """
        This class is the representation of the cards that each player has in their hands, it gets passed the overall
        deck, but each player has a different player deck
        :param named_player: The player's name
        :param deck: The overall deck of cards
        :param human_player: A True or false that is used to determine if the player is a human or not
        """
        self.card_list = [[], [], [], [], [], [], [], [], [], []]
        self.player_name = named_player
        self.deck = deck
        self.human_player = human_player

    def take_card(self, taken_card):
        card_to_replace = input(
            "what card do you want to replace?: (Type 11 if you want to return the card just picked up): ")
        if card_to_replace == "11":
            return None
        self.card_list.pop(int(card_to_replace) - 1)
        self.card_list.insert(int(card_to_replace) - 1, taken_card)

    def ai_take_card(self, taken_card, replace_num):
        if replace_num == 11:
            return None
        self.card_list.pop(int(replace_num) - 1)
        self.card_list.insert(int(replace_num) - 1, taken_card)

    def deal(self):
        self.card_list = self.deck.deal_deck()


def create_ascii_deck_cards(n1: int):
    """
    creates the ascii card for the <<face up face down>> configuration
    :param n1: the number that represents the card
    """
    suits = ["♠", "♡", "♣", "♢"]
    suit = n1 // 100
    if 1 <= n1 - suit * 100 <= 9:
        space_deck_cards = ' '
    else:
        space_deck_cards = ''
    card = (
        'You can take the known card or the unknown card\n'
        '┌─────────┐     ┌─────────┐\n'
        f'│{n1 - suit * 100}    {space_deck_cards}   │     │░░░░░░░░░│\n'
        '│         │     │░░░░░░░░░│\n'
        '│         │     │░░░░░░░░░│\n'
        f'│    {suits[suit - 1]}    │     │░░░░░░░░░│\n'
        '│         │     │░░░░░░░░░│\n'
        '│         │     │░░░░░░░░░│\n'
        f'│ {space_deck_cards}      {n1 - suit * 100}│     │░░░░░░░░░│\n'
        '└─────────┘     └─────────┘\n')
    print(card)


def create_ascii_player_cards(player_cards_list, player_name_display):
    """
    creates the full hand of the cards for the player
    :param player_cards_list: the list of cards the player has
    :param player_name_display: the player's name
    """
    full_lines = [[], [], [], [], [], [], [], [], [], []]
    suits = ["♠", "♡", "♣", "♢"]
    card_num = 0
    for num in player_cards_list:
        suit = num // 100
        if not 1 <= num - suit * 100 <= 9:
            space = ''
        else:
            space = ' '

        lines = [
            ['┌─────────┐'],
            [f'│{num - suit * 100}       {space}│'],
            ['│         │'],
            ['│         │'],
            [f'│    {suits[suit - 1]}    │'],
            ['│         │'],
            ['│         │'],
            [f'│{space}       {num - suit * 100}│'],
            ['└─────────┘'],
            [f' card #{card_num + 1}   ']
        ]
        card_num = card_num + 1
        for line in range(10):
            full_lines[line] = full_lines[line] + lines[line]
    print(f"These Are your Cards {player_name_display}")
    for line_print in full_lines:
        print(line_print)


def show_single_card(num, message):
    suits = ["♠", "♡", "♣", "♢"]
    suit = num // 100
    if 1 <= num - suit * 100 <= 9:
        space = ' '
    else:
        space = ''
    lines = [
        ['┌─────────┐'],
        [f'│{num - suit * 100}       {space}│'],
        ['│         │'],
        ['│         │'],
        [f'│    {suits[suit - 1]}    │'],
        ['│         │'],
        ['│         │'],
        [f'│{space}       {num - suit * 100}│'],
        ['└─────────┘'],
        [f'{message}']
    ]
    for print_line in lines:
        print(print_line)


def unicode(card_list):
    """
    added mostly as a joke, these are almost impossible to read
    :param card_list: the list of player cards
    """
    from unicards import unicard
    suits = ["s", "h", "c", "d"]
    print_list = []
    for num in card_list:
        if not num == 520:
            suit = num // 100
            num = num - suit * 100
            if 2 <= num <= 9:
                print_list.append(unicard(f"{num}{suits[suit - 1]}"))
            elif num == 10:
                print_list.append(unicard(f"t{suits[suit - 1]}"))
            elif num == 11:
                print_list.append(unicard(f"J{suits[suit - 1]}"))
            elif num == 12:
                print_list.append(unicard(f"Q{suits[suit - 1]}"))
            elif num == 13:
                print_list.append(unicard(f"K{suits[suit - 1]}"))
            elif num == 1:
                print_list.append(unicard(f"A{suits[suit - 1]}"))
        else:
            print_list.append("🂠")
    print(print_list)
    if len(card_list) == 10:
        print(list(range(1, 10 + 1)))


class game_loop:
    @input_safe
    def __init__(self):
        """
        The actual game loop
        """
        print(text2art("Gin Rummy!", font="varsity"))
        ascii_or_uni = input(
            "Do you want to use ASCII cards or unicode cards? (ASCII is much easier to read) \n 1 = ASCII, "
            "2 = unicode\n:")
        if ascii_or_uni == "2":
            self.ascii_or_uni = False
        else:
            self.ascii_or_uni = True
        self.card_deck = PlayingCard(int(input("How many decks do you want to play with?\n: ")))
        self.human_players = input("How many human players?\n: ")
        self.computer_players = input("How many computer players?\n: ")
        self.player_classes = []
        self.game_on = True
        self.game_off_soon = False

        for x in range(int(self.human_players)):
            player_name = input(f"what is player {x + 1}s name?: ")
            self.player_classes.append(playerDeck(player_name, self.card_deck, True))

        for computer_num in range(int(self.computer_players)):
            self.player_classes.append(playerDeck(f"computer {computer_num + 1}", self.card_deck, False))

        for player in self.player_classes:
            player.deal()

        self.player_message = ""

    @input_safe
    def human_interaction(self, player):
        """
        gives prompts to the player to play
        :param player: the player that is playing
        """
        print(f"Pass the computer to {player.player_name} \n Press any button to continue")
        input("")
        print(self.player_message)
        if self.ascii_or_uni:
            create_ascii_deck_cards(self.card_deck.card_on_top)
            create_ascii_player_cards(player.card_list, player.player_name)
        else:
            print("You can take the known card or the unknown card")
            unicode([self.card_deck.card_on_top, 520])
            unicode(player.card_list)
        action = input(
            "What do you want to do?\n 1 = Take the known card \n 2 = Take the unknown card \n 3 = other \n :")
        if action == "1":
            card_taken = self.card_deck.take_the_known_card()
            if self.ascii_or_uni:
                show_single_card(card_taken, "This is the card you drew")
            else:
                print("This is the card you drew")
                unicode([card_taken])
            player.take_card(card_taken)
        elif action == "2":
            card_taken = self.card_deck.draw_unknown_card()
            if self.ascii_or_uni:
                show_single_card(card_taken, "This is the card you drew")
            else:
                print("This is the card you drew")
                unicode([card_taken])
            player.take_card(card_taken)
        elif action == "3":
            human_game_action = input("1 = end game, 2 = or declare last turn, 3 = Reshuffle Discard")
            if human_game_action == "1":
                self.game_on = False
            elif human_game_action == "2":
                self.player_message = "LAST TURN"
                self.game_off_soon = True
            elif human_game_action == "3":
                if self.card_deck.check_empty_deck():
                    print("Deck has been reshuffled")
                    self.player_message = "The Deck was reshuffled"
            self.card_deck.check_empty_deck()
        os.system('cls' if os.name == 'nt' else "printf '\033c'")

    def computer_interaction(self, player):
        """
        processes the computer's wishes
        :param player: the computers deck
        """
        face_up_card = self.card_deck.card_on_top
        computer_cards = player.card_list
        create_ascii_player_cards(computer_cards, "Computer")
        AI = ai(face_up_card, computer_cards)
        # The computer initially does a markov decision-making process to decide whether it is worth
        # taking a known card or taking an unknown card. 1 is a known card, 2 is an unknown card, 3 knocking to
        # win the game
        action = AI.carlo()
        print(f"\nthe Ai chose {action}")
        if action == 1:
            # after deciding what card to take, the computer chooses witch card is the least valuable,
            # it will choose 11 if it wants to change nothing
            card_taken = self.card_deck.take_the_known_card()
            print("The AI decided to take the known card")
            replace_card = AI.replace_choice(computer_cards, card_taken)
            player.ai_take_card(card_taken, replace_card)
            if replace_card != 11:
                self.card_deck.discard(player.card_list[replace_card])
        elif action == 2:
            card_taken = self.card_deck.draw_unknown_card()
            print("The AI decided to take the unknown card")
            replace_card = AI.replace_choice(computer_cards, card_taken)
            player.ai_take_card(card_taken, replace_card)
            if replace_card != 11:
                self.card_deck.discard(player.card_list[replace_card])
        elif action == 3:
            print("The AI knocked")
            self.player_message = "!!!LAST TURN!!!"
            self.game_off_soon = True


    def primary_loop(self):
        """
        basic loop to keep the game running
        """
        if self.game_off_soon:
            self.game_on = False
        self.card_deck.check_empty_deck()

        for player in self.player_classes:
            if player.human_player:
                self.human_interaction(player)
            else:
                self.computer_interaction(player)

    def determine_hand_value(self, player_cards):
        """
        Determines the hand value for the end of the game; this is just to decide which player won.
        Is nearly the
        exact same as the cost function in AI.py
        but with determining the value of the hand held not the cost of the
        hand
        :param player_cards: list of player cards
        :return: value of hand
        """
        from itertools import groupby
        from operator import itemgetter
        def card_val(imp_):
            val = 0
            if imp_ == 1:
                val += 15
            elif imp_ < 9:
                val += 5
            elif imp_ > 9:
                val += 10
            return val

        hand_value = 0
        cost_cards = player_cards.copy()

        # finding cards that are in a set and removing them from the cost
        cards_sorted_set = player_cards.copy()
        for count, card in enumerate(cards_sorted_set):
            cards_sorted_set[count] = card - ((card // 100) * 100)
        cards_sorted_set = {i: cards_sorted_set.count(i) for i in cards_sorted_set}
        for value, key in cards_sorted_set.items():
            if value > 2:
                for it in cost_cards:
                    if it - ((it // 100) * 100) == value:
                        hand_value += card_val(it)

        # finding cards that are in a straight and removing them from the cost
        sorted_straight = []
        for k, g in groupby(enumerate(cost_cards), lambda ix: ix[0] - ix[1]):
            sorted_straight.append(list(map(itemgetter(1), g)))

        for cards in sorted_straight:
            if len(cards) > 2:
                for card in cards:
                    hand_value += card_val(card)
        return hand_value

    def determine_winner(self):
        """
        just loops through the players and finds out who won
        :return: a string declaring the winner
        """
        winner = ""
        winning_cost = -math.inf
        for player in self.player_classes:
            player_value = self.determine_hand_value(player.card_list)
            if player_value > winning_cost:
                winning_cost = player_value
                winner = f"winning player is {player.player_name}"
        return winner
