import math
from itertools import groupby, filterfalse
from operator import itemgetter
import numpy as np
import random

import game


class Node:
    def __init__(self, value, cost=0, parent=None, action=None):
        self.cost = cost
        self.parent = parent
        self.path = []
        self.value = value
        self.children = []
        self.action = action


class ai:
    def __init__(self, vis_card, player_cards):
        self.vis_card = vis_card
        self.init = Node(player_cards)

    def carlo(self) -> int:
        """
        Simple markov process, it decides if taking the visible card will reduce the cost
        of its current hand.
        If it does, it chooses to take that card, if it doesn't, it takes the hidden card.
        If the current hand is below the threshold, it chooses to end the game.
        :return: An int representing the AI's choice
        """
        gamma = .9
        threshold = random.randint(11, 31)
        if self.cost(self.init.value) < threshold:
            return 3
        self.init.children = self.generate_tree(self.vis_card)
        max_val = (0,-1)
        for child in self.init.children:
            for val in range(10):
                q = [0] * 11
                reward = abs(self.cost(child.value) - self.cost(self.init.value))
                q[val + 1] = reward + gamma * (1 / 42 * q[val])
                if max(q) > max_val[0]:
                    if max(q) <= 15:
                        activation = 1
                    else:
                        activation = 2
                    max_val = (max(q), activation)
        return int(max_val[1])

    def cost(self, player_cards) -> (int, int):
        """
        determines the cost of the hand it is passed, higher cost is bad; in rummy it is called deadwood
        :param player_cards: list of player cards
        :return: the cost of the current hand
        """
        cost = 0
        cost_cards = player_cards.copy()

        # finding cards that are in a set and removing them from the cost
        cards_sorted_set = player_cards.copy()
        for count, card in enumerate(cards_sorted_set):
            cards_sorted_set[count] = card - ((card // 100) * 100)  # Magic to get rid of the suit
        cards_sorted_set = {i: cards_sorted_set.count(i) for i in
                            cards_sorted_set}  # count the number of each card value
        for key, value in cards_sorted_set.items():
            if value > 1:
                for val, it in enumerate(cost_cards):
                    if it - ((it // 100) * 100) == value:
                        cost_cards.pop(val)

        # finding cards that are in a straight and removing them from the cost
        sorted_straight = []
        for k, g in groupby(enumerate(cost_cards), lambda ix: ix[0] - ix[1]):  # I wrote this and I don't understand it
            g = list(g)
            sorted_straight.append(list(map(itemgetter(1), g)))  # Somehow lists all sequential cards in the list

        for cards in sorted_straight:
            if len(cards) > 1:
                cost_cards = list(set(cost_cards).difference(cards))
        for card in cost_cards:
            card = card - ((card // 100) * 100)
            if card == 1:
                cost += 15
            elif card < 9:
                cost += 5
            elif card > 9:
                cost += 10

        return cost

    def policy(self, player_cards_lst):
        pass

    def replace_choice(self, player_cards, card) -> int:
        """
        finds the card(s) that would be most advantageous to replace with the card-passed
        :param player_cards: list of current cards
        :param card: card that is replacing
        :return: a number between 1-11 that represents the location of the card to replace with the card-passed
        """
        tree = []
        for loc in range(len(player_cards)):
            player_cards_copy = player_cards.copy()
            player_cards_copy[loc] = card
            if self.cost(player_cards_copy) < self.cost(player_cards):
                tree.append(loc)
        if len(tree) == 0:
            return 11
        return tree[random.randint(0, len(tree) - 1)]

    def generate_tree(self, vis_card):
        """
        generates a node tree of cards
        :param vis_card: the visible card that can replace any card in the hand
        :return:
        """
        tree = []
        for card in self.init.value:
            for loc in range(len(self.init.value)):
                new_value = self.init.value.copy()
                new_value[loc] = vis_card
                cost = self.cost(new_value)
                new_node = Node(value=new_value, parent=card, cost=cost, action=loc)
                tree.append(new_node)
        return tree
