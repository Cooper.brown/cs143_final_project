from game import game_loop


game_loop = game_loop()
while game_loop.game_on:
    game_loop.primary_loop()
print(game_loop.determine_winner())

